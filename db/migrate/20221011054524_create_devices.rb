class CreateDevices < ActiveRecord::Migration[7.0]
  def change
    create_table :devices do |t|
      t.string :name
      t.string :brand
      t.string :nodel
      t.text :serial
      t.text :stock
      t.boolean :active
      t.text :department
      t.text :location

      t.timestamps
    end
  end
end
