class CreateRepairs < ActiveRecord::Migration[7.0]
  def change
    create_table :repairs do |t|
      t.string :worker
      t.integer :time
      t.text :description
      t.boolean :finished

      t.timestamps
    end
  end
end
